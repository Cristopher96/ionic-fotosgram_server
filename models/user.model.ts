import { Schema, model, Document } from 'mongoose';
import bcrypt from 'bcrypt';

const userSchema = new Schema({
    name: {
        type: String,
        required:[true, 'Field Name is obligatory.']
    },
    avatar: {
        type: String,
        default: 'av-1.png'
    },
    email: {
        type: String,
        unique: true,
        required: [true, 'Field Email is obligatory.']
    },
    password: {
        type: String,
        required: [true, 'Field Password is obligaory.']
    }
});

userSchema.method('comparePassword', function(password: string = ''): boolean {
    if (bcrypt.compareSync(password , this.password)) {
        return true;
    } else {
        return false;
    }
});

interface IUser extends Document {
    name: string;
    email: string;
    password: string;
    avatar: string;

    comparePassword(password: string):boolean;
}

export const User = model<IUser>('User', userSchema);