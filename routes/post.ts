import { Router, Response } from 'express';
import { verifyToken } from '../middlewares/auth';
import { Post } from '../models/post.model';
import { FileUpload } from '../interfaces/file-upload';
import FileSystem from '../classes/file-system';

const postRoutes = Router();
const fileSystem = new FileSystem();

// get posts
postRoutes.get('/', async (req: any, res: Response) => {
    let page = Number(req.query.page) || 1;
    let skip = page - 1;
    skip = skip * 10;
    const posts = await Post.find().sort({_id: '-1'}).skip(skip).limit(10).populate('user', '-password').exec();
    res.json({
        ok:true,
        page,
        posts
    })
});

// create posts
postRoutes.post('/', [verifyToken], (req: any, res: Response) => {
    const body = req.body;
    body.user = req.user._id;

    const images = fileSystem.imagesFromTempToPosts(req.user._id);
    body.imgs = images;

    Post.create(body).then(async postDB => {
        await postDB.populate('user', '-password').execPopulate();        
        res.json({
            ok: true,
            post: postDB
        });  
    }).catch(err => {
        res.json(err);
    });    
});

postRoutes.post('/upload', [verifyToken], async (req: any, res: Response) => {
    if (!req.files) {
        return res.status(400).json({
            ok: false,
            message: `We couldn't upload the files.`
        });
    }

    const file: FileUpload = req.files.img;

    if (!file) {
        return res.status(400).json({
            ok: false,
            message: `We couldn't upload the files. image`
        });
    }

    if (!file.mimetype.includes('image')) {
        return res.status(400).json({
            ok: false,
            message: `What you upload isn't an image`
        });
    }

    await fileSystem.saveTempImages(file, req.user._id);

    res.json({
        ok: true,
        file: file.mimetype
    });
});

// get image
postRoutes.get('/image/:userid/:img', (req: any, res: Response) => {
    const userId = req.params.userid;
    const img    = req.params.img;
    const photoPath = fileSystem.getPhotoUrl(userId, img);
    res.sendFile(photoPath);
});

export default postRoutes;