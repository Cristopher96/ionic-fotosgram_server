"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const auth_1 = require("../middlewares/auth");
const post_model_1 = require("../models/post.model");
const file_system_1 = __importDefault(require("../classes/file-system"));
const postRoutes = express_1.Router();
const fileSystem = new file_system_1.default();
// get posts
postRoutes.get('/', (req, res) => __awaiter(this, void 0, void 0, function* () {
    let page = Number(req.query.page) || 1;
    let skip = page - 1;
    skip = skip * 10;
    const posts = yield post_model_1.Post.find().sort({ _id: '-1' }).skip(skip).limit(10).populate('user', '-password').exec();
    res.json({
        ok: true,
        page,
        posts
    });
}));
// create posts
postRoutes.post('/', [auth_1.verifyToken], (req, res) => {
    const body = req.body;
    body.user = req.user._id;
    const images = fileSystem.imagesFromTempToPosts(req.user._id);
    body.imgs = images;
    post_model_1.Post.create(body).then((postDB) => __awaiter(this, void 0, void 0, function* () {
        yield postDB.populate('user', '-password').execPopulate();
        res.json({
            ok: true,
            post: postDB
        });
    })).catch(err => {
        res.json(err);
    });
});
postRoutes.post('/upload', [auth_1.verifyToken], (req, res) => __awaiter(this, void 0, void 0, function* () {
    if (!req.files) {
        return res.status(400).json({
            ok: false,
            message: `We couldn't upload the files.`
        });
    }
    const file = req.files.img;
    if (!file) {
        return res.status(400).json({
            ok: false,
            message: `We couldn't upload the files. image`
        });
    }
    if (!file.mimetype.includes('image')) {
        return res.status(400).json({
            ok: false,
            message: `What you upload isn't an image`
        });
    }
    yield fileSystem.saveTempImages(file, req.user._id);
    res.json({
        ok: true,
        file: file.mimetype
    });
}));
// get image
postRoutes.get('/image/:userid/:img', (req, res) => {
    const userId = req.params.userid;
    const img = req.params.img;
    const photoPath = fileSystem.getPhotoUrl(userId, img);
    res.sendFile(photoPath);
});
exports.default = postRoutes;
