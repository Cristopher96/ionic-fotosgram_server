"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
const uniqid_1 = __importDefault(require("uniqid"));
class FileSystem {
    constructor() { }
    saveTempImages(file, userId) {
        return new Promise((resolve, reject) => {
            // create directorys
            const path = this.createUserDir(userId);
            // file name
            const fileName = this.uniqueFileName(file.name);
            // move the file from temp to our directory
            file.mv(`${path}/${fileName}`, (err) => {
                if (err) {
                    reject(err);
                }
                else {
                    resolve();
                }
            });
        });
    }
    uniqueFileName(ogName) {
        const nameArr = ogName.split('.');
        const ext = nameArr[nameArr.length - 1];
        const uniqname = uniqid_1.default();
        return `${uniqname}.${ext}`;
    }
    createUserDir(userId) {
        const userPath = path_1.default.resolve(__dirname, '../uploads/', userId);
        const userTempPath = userPath + '/temp';
        const exists = fs_1.default.existsSync(userPath);
        if (!exists) {
            fs_1.default.mkdirSync(userPath);
            fs_1.default.mkdirSync(userTempPath);
        }
        return userTempPath;
    }
    imagesFromTempToPosts(userId) {
        const tempPath = path_1.default.resolve(__dirname, '../uploads/', userId, 'temp');
        const postPath = path_1.default.resolve(__dirname, '../uploads/', userId, 'posts');
        if (!fs_1.default.existsSync(tempPath)) {
            return [];
        }
        if (!fs_1.default.existsSync(postPath)) {
            fs_1.default.mkdirSync(postPath);
        }
        const imagesTemp = this.getImagesInTemp(userId);
        imagesTemp.forEach(image => {
            fs_1.default.renameSync(`${tempPath}/${image}`, `${postPath}/${image}`);
        });
        return imagesTemp;
    }
    getImagesInTemp(userId) {
        const tempPath = path_1.default.resolve(__dirname, '../uploads/', userId, 'temp');
        return fs_1.default.readdirSync(tempPath) || [];
    }
    getPhotoUrl(userId, img) {
        const photoPath = path_1.default.resolve(__dirname, '../uploads', userId, 'posts', img);
        const exists = fs_1.default.existsSync(photoPath);
        if (!exists) {
            return path_1.default.resolve(__dirname, '../assets/400x250.jpg');
        }
        return photoPath;
    }
}
exports.default = FileSystem;
