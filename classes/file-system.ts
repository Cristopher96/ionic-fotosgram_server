import { FileUpload } from '../interfaces/file-upload';
import path from 'path';
import fs from 'fs';
import uniqid from 'uniqid';

export default class FileSystem {
    constructor() {}

    saveTempImages(file: FileUpload, userId: string) {
        return new Promise((resolve, reject) => {
            // create directorys
            const path = this.createUserDir(userId);

            // file name
            const fileName = this.uniqueFileName(file.name);
            
            // move the file from temp to our directory
            file.mv(`${path}/${fileName}`, (err: any) => {
                if (err) {
                    reject(err);
                } else {
                    resolve();
                }
            })
        })
    }

    private uniqueFileName(ogName: string) {
        const nameArr = ogName.split('.');
        const ext = nameArr[nameArr.length - 1];
        const uniqname = uniqid();
        return `${uniqname}.${ext}`;
    }

    private createUserDir(userId: string) {
        const userPath = path.resolve(__dirname, '../uploads/', userId);
        const userTempPath = userPath + '/temp';
        const exists = fs.existsSync(userPath);

        if (!exists) {
            fs.mkdirSync(userPath);
            fs.mkdirSync(userTempPath);
        }
        return userTempPath;
    }

    imagesFromTempToPosts(userId: string) {
        const tempPath = path.resolve(__dirname, '../uploads/', userId, 'temp');
        const postPath = path.resolve(__dirname, '../uploads/', userId, 'posts');
        if (!fs.existsSync(tempPath)) {
            return [];
        }
        if (!fs.existsSync(postPath)){
            fs.mkdirSync(postPath);
        }

        const imagesTemp = this.getImagesInTemp(userId);
        imagesTemp.forEach(image => {
            fs.renameSync(`${tempPath}/${image}`, `${postPath}/${image}`);
        });
        return imagesTemp;
    }

    private getImagesInTemp(userId: string) {
        const tempPath = path.resolve(__dirname, '../uploads/', userId, 'temp');
        return fs.readdirSync(tempPath) || [];
    }

    getPhotoUrl(userId: string, img: string) {
        const photoPath = path.resolve(__dirname, '../uploads', userId, 'posts', img);
        

        const exists = fs.existsSync(photoPath);
        if (!exists) {
            return path.resolve(__dirname, '../assets/400x250.jpg');
        }

        return photoPath;
    }
}