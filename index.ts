import Server from './classes/server';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import fileUpload from 'express-fileupload';
import userRoutes from './routes/user';
import postRoutes from './routes/post';
import cors from 'cors';

const server = new Server();

// body parser
server.app.use(bodyParser.urlencoded({extended: true}));
server.app.use(bodyParser.json());

// file upload
server.app.use(fileUpload());

// CORS Config
server.app.use(cors({origin: true, credentials: true}));

// app's routes
server.app.use('/user', userRoutes);
server.app.use('/posts', postRoutes);

// connect DB
mongoose.connect('mongodb://localhost:27017/fotosgram',
    {
        useNewUrlParser: true,
        useCreateIndex: true
    }, (err) => {
        if (err) throw err;
        console.log('Database ONLINE');
});

// starting the server
server.start();